CREATE TYPE w_time AS (t1 time, t2 time);

CREATE TABLE cashbox (
	id_cashbox			serial PRIMARY KEY,
	city				varchar(20) NOT NULL,
	street				varchar(20) NOT NULL,
	house_number		int NOT NULL,
	working_time		w_time NOT NULL 
);

CREATE TABLE cashiers (
	id_cashier			serial PRIMARY KEY,
	id_cashbox			int REFERENCES cashbox,
	working_time		w_time NOT NULL,
	name				varchar(25) NOT NULL,
	surname				varchar(25) NOT NULL,
	phone_number		varchar(13) NOT NULL
);

CREATE TABLE consumers_facilities (
	id_facility			serial PRIMARY KEY,
	facility			varchar(10) NOT NULL,
	facility_percent	float CHECK (facility_percent >=0 AND facility_percent <= 1) NOT NULL --2
);

CREATE TABLE consumers (
	id_consumer			serial PRIMARY KEY,
	name				varchar(25) NOT NULL,
	passport_series		char(2),
	passport_number		int,
	id_facility			smallint REFERENCES consumers_facilities
);

CREATE TABLE stations (
	id_station			serial PRIMARY KEY,
	name				varchar(25) NOT NULL
);

CREATE TABLE trains (
	train_number		smallint PRIMARY KEY,
	departure_point		int REFERENCES stations,
	arrival_point		int REFERENCES stations,
	departure_time		time NOT NULL,
	arrival_time		time NOT NULL,
	travel_days			smallint CHECK (travel_days >= 0) --3
);

CREATE TABLE carriage_type (
	id_carriage_type	serial PRIMARY KEY,
	type				varchar(25) NOT NULL,
	price_per_km		float CHECK (price_per_km > 0) --4
);

CREATE TABLE carriages (
	id_carriage			serial PRIMARY KEY,
	id_carriage_type	int REFERENCES carriage_type,
	train_number		int REFERENCES trains,
	tickets_number		smallint CHECK (tickets_number >= 0), --5
	carriage_number		smallint CHECK (carriage_number > 0) --6
);

CREATE TABLE shedules (
	id_shedule			serial PRIMARY KEY,
	train_number		int REFERENCES trains,
	departure_time		time NOT NULL,
	arrival_time		time NOT NULL,
	id_station			int REFERENCES stations,
	station_sequence	int CHECK (station_sequence > 0), --7
	previous_st_destination	int CHECK (previous_st_destination > 0)--8
);

CREATE TABLE tickets_conditions (
	id_condition		serial PRIMARY KEY,
	condition			varchar(10) NOT NULL
);

CREATE TABLE tickets (
	id_ticket			serial PRIMARY KEY,
	id_consumer			int REFERENCES consumers,
	id_carriage			int REFERENCES carriages,
	carriage_number		smallint REFERENCES carriages,
	place				smallint NOT NULL,
	price				float CHECK (price > 0), --9
	departure_station	int REFERENCES stations,
	arrival_station		int REFERENCES stations,
	departure_date		date NOT NULL,
	arrival_date		date NOT NULL,
	id_condition		smallint REFERENCES tickets_conditions,
	CHECK (departure_date < arrival_date) --10
);

CREATE TABLE reports (
	id_report			serial PRIMARY KEY,
	revenue				float CHECK (revenue >= 0),
	sold_tickets		int CHECK (sold_tickets >= 0),
	returned_tickets	int CHECK (returned_tickets >= 0),
	period				interval NOT NULL,
	CHECK (returned_tickets <= sold_tickets)
);

CREATE TABLE trains_timetable (
	train_number smallint REFERENCES trains,
	departure_date	date NOT NULL
);




