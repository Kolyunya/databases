INSERT INTO cashbox VALUES (1, 'Kiev', 'Ivanova', 12, '(08:00, 16:00)'), (1, 'Kiev', 'Petrova', 14, '10:00'); --Неверный тип данных
INSERT INTO cashiers VALUES (1, 1, '08:00,12:00', 'Vasya', , '+380666666666'), ('2', '2', '(12:00,16:00)', 'Petya', 'Sidorov', '+380777777777'); --Пустое значение
INSERT INTO consumers_facilities VALUES (1, 'STUDENT', '0.5'), (2, 'CHILD', '2'); 
INSERT INTO consumers VALUES (1, 'Kolya', 'AF', '124112', 1), (2, 'Seryozha', 'JH', '211431', 3);
INSERT INTO stations VALUES ('1', 'Kyiv'), ('2', 'Dnipropetrovsk'), ('3', 'Zaporizhya');
INSERT INTO trains VALUES ('72','1','3','20:28','07:08','1'), ('80','1','2','23:16','07:43', '1');
INSERT INTO carriage_type VALUES ('1', 'Platskart','0.17'), ('2', 'Coupe', '0.3'), ('3','Lux','0.51');
INSERT INTO carriages VALUES ('1', '1', '72', '52', '1'), ('2', '1', '72', '52', '2'), ('3', '1', '72', '52', '3'), ('4', '2', '72', '52', '4');
INSERT INTO shedules VALUES ('1','72','04:40','05:00','3','2','400')
INSERT INTO tickets_conditions VALUES (1, 'SOLD'), (2, 'BOOKED');
INSERT INTO tickets VALUES ('2', '1', '1', '1', '1', '-100', '1', '2' ,'2013-03-30', '2013-03-31', 1); --отрицательное значение
INSERT INTO reports VALUES ('1','100','1','0','3');
INSERT INTO trains_timetable VALUES ('72', '2013-03-30');
