SELECT cbx.id_cashbox, csh.id_cashier, csh.name, csh.surname, csh.phone_number
FROM cashbox cbx
INNER JOIN cashiers csh
ON csh.id_cashbox = cbx.id_cashbox
WHERE cbx.id_cashbox = 1;

SELECT tr.train_number, dep.id_station, dep.name, arr.id_station, arr.name FROM trains tr
INNER JOIN 
	(SELECT id_station, name FROM stations) AS dep ON tr.departure_point = dep.id_station
INNER JOIN
	(SELECT id_station, name FROM stations) AS arr ON tr.arrival_point = arr.id_station
WHERE dep.id_station = 1 AND arr.id_station = 2;

SELECT ct.id_carriage_type, SUM(car.tickets_number) FROM carriages car
INNER JOIN carriage_type ct ON car.id_carriage_type = ct.id_carriage_type
WHERE ct.id_carriage_type = 1 AND car.train_number = 72
GROUP BY ct.id_carriage_type;

SELECT t.id_ticket, t.id_carriage, t.place, t.price, t.departure_date, t.arrival_date
FROM tickets t
INNER JOIN consumers c ON c.id_consumer = t.id_consumer
LEFT JOIN consumers_facilities cf ON c.id_facility = cf.id_facility
INNER JOIN carriages car ON car.id_carriage = t.id_carriage
INNER JOIN carriage_type car_t ON car.id_carriage_type = car_t.id_carriage_type
INNER JOIN tickets_conditions tc ON t.id_condition = tc.id_condition;

 

